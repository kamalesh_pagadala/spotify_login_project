/*Step definition file for Login functionality*/

package step_definitions;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test_modules.LoginModule;
import test_page_objects.AccountOverviewPage;
import test_page_objects.HomePage;
import test_page_objects.LoginPage;

public class LoginSteps {
	public WebDriver driver;

	public LoginSteps() {
		driver = BaseHooks.driver;
		PageFactory.initElements(driver, HomePage.class);
		PageFactory.initElements(driver, LoginPage.class);
		PageFactory.initElements(driver, AccountOverviewPage.class);
	}

	@Given("^I visit the spotify home page$")
	public void go_to_home_page() throws Throwable {
		driver.get("https://www.spotify.com/us/");
		Assert.assertEquals("Music for everyone - Spotify", driver.getTitle());
	}

	@When("^I navigate to the Spotify Login page$")
	public void go_to_login_page() throws Throwable {
		LoginModule.navigate_to_login_page(driver);
		Assert.assertEquals("Login - Spotify", driver.getTitle());
	}

	@When("^I login with username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void login(String username, String password) throws Throwable {
		LoginModule.execute_login(driver, username, password);
	}

	@Then("^I logout successfully$")
	public void logout() throws Throwable {
		LoginModule.execute_logout(driver);
		Assert.assertEquals("Music for everyone - Spotify", driver.getTitle());
	}

	@Then("^the login is successful$")
	public void login_successful() throws Throwable {
		LoginModule.account_page_header_dropdown_displayed();
		Assert.assertEquals("Account overview - Spotify", driver.getTitle());
	}

	@Then("^the login is unsuccessful$")
	public void login_unsuccessful() throws Throwable {
		LoginModule.login_alert_warning_displayed();
		Assert.assertEquals("Incorrect username or password.", LoginPage.username_password_alert_warning.getText());
	}
}