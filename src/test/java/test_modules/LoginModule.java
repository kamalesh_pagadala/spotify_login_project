/*Module file for Login functionality*/
package test_modules;

import org.openqa.selenium.WebDriver;

import test_helpers.Log;
import test_page_objects.LoginPage;
import test_page_objects.AccountOverviewPage;
import test_page_objects.HomePage;

public class LoginModule {

	public static void execute_login(WebDriver driver, String username, String password) throws Exception {

		Log.info("Logging in with username and password");

		LoginPage.username.sendKeys(username);
		Log.info("username entered");

		LoginPage.password.sendKeys(password);
		Log.info("password entered");

		LoginPage.login_button.click();
		Log.info("click action is performed on login");
	}

	public static void account_page_header_dropdown_displayed() {
		AccountOverviewPage.wait_header_dropdown_displayed();
	}

	public static void login_alert_warning_displayed() {
		LoginPage.wait_login_alert_warning_displayed();
	}

	public static void navigate_to_login_page(WebDriver driver) throws Exception {
		
		HomePage.log_in_link.click();
		Log.info("Click action performed on Log in link in Home page");
	}

	public static void execute_logout(WebDriver driver) throws Exception {

		AccountOverviewPage.header_dropdown.click();
		AccountOverviewPage.wait_log_out_link_displayed();
		AccountOverviewPage.log_out_link.click();
		Log.info("click action is performed on logout");

	}
}