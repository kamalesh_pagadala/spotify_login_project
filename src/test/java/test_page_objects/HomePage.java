/*Page class for Spotify Home page*/
package test_page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import test_page_objects.BaseClass;

public class HomePage extends BaseClass {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	@FindBy(how = How.LINK_TEXT, using = "Sign up")
	public static WebElement sign_up;

	@FindBy(how = How.ID, using = "header-login-link")
	public static WebElement log_in_link;

	public static String home_page_url = "https://www.spotify.com/us/";

	public static void navigate() {
		driver.get(home_page_url);
	}
}