/*Page class for Login page*/
package test_page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import test_page_objects.BaseClass;

public class LoginPage extends BaseClass {

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(how = How.ID, using = "login-username")
	public static WebElement username;

	@FindBy(how = How.ID, using = "login-password")
	public static WebElement password;

	@FindBy(how = How.CSS, using = "button.btn-green")
	public static WebElement login_button;

	@FindBy(how = How.CSS, using = "p.alert.alert-warning span")
	public static WebElement username_password_alert_warning;

	String login_page_url = "https://accounts.spotify.com/en-US/login";

	public void navigate() {
		driver.get(login_page_url);
	}

	public static void wait_login_alert_warning_displayed() {
		BaseClass.waitForVisibility(username_password_alert_warning);
	}
}