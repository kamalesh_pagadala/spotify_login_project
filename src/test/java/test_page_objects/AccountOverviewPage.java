/*Page class for Account overview page*/
package test_page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AccountOverviewPage extends BaseClass {

	public AccountOverviewPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(how = How.CLASS_NAME, using = "svg-chevron-down")
	public static WebElement header_dropdown;

	@FindBy(how = How.CSS, using = "#navbar-nav ul li.dropdown.alternate.hidden-sidepanel.open ul li:nth-child(3) a")
	public static WebElement log_out_link;

	public static String account_overview_page_url = "https://www.spotify.com/us/account/overview/";

	public static void navigate() {
		driver.get(account_overview_page_url);
	}

	public static void wait_header_dropdown_displayed() {
		BaseClass.waitForVisibility(header_dropdown);
	}

	public static void wait_log_out_link_displayed() {
		BaseClass.waitForVisibility(log_out_link);
	}
}