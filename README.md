Author: Kamalesh Pagadala

Spotify Basic Login feature test cases:

Test case 1: Successful user login
Verify if the user is able to login with correct username and password.

Steps:
Navigate to “https://www.spotify.com/us/” home page
Click on the “Log in” link in the header bar
Enter the given test data in ‘User name’ and ‘password’ fields and click on ‘Log in’ button.

Expected result:
The user is able to login successfully, and lands on the ‘Account overview’ page

Test data: 
User name/password: myspotifytestuser@gmail.com / spotify123

Test case 2: Unsuccessful user login with incorrect password
Verify if the user is able to login with correct username and incorrect password.

Steps:
Navigate to “https://www.spotify.com/us/” home page
Click on the “Log in” link in the header bar
Enter the given test data in ‘User name’ and ‘password’ fields and click on ‘Log in’ button.

Expected result:
The user is unable to login successfully, and the following error message is displayed - “Incorrect username or password”

Test data: 
User name/password: myspotifytestuser@gmail.com / spotify123333333

Test case 3: Unsuccessful user login with incorrect username
Verify if the user is able to login with incorrect username and correct password.

Steps:
Navigate to “https://www.spotify.com/us/” home page
Click on the “Log in” link in the header bar
Enter the given test data in ‘User name’ and ‘password’ fields and click on ‘Log in’ button.

Expected result:
The user is unable to login successfully, and the following error message is displayed - “Incorrect username or password”

Test data: 
User name/password: incorrectuser@gmail / spotify123

Test case 4: Unsuccessful user login with correct username and no password
Verify if the user is able to login with correct username and no password.

Steps:
Navigate to “https://www.spotify.com/us/” home page
Click on the “Log in” link in the header bar
Enter the correct username in the username field and click on ‘Log in’ button.

Expected result:
The user is unable to login successfully, and the following error message is displayed below the password field - “Please enter your password”

Test data: 
User name/password: myspotifytestuser@gmail.com

Test case 5: Unsuccessful user login with correct password and no username
Verify if the user is able to login with correct password and no username.

Steps:
Navigate to “https://www.spotify.com/us/” home page
Click on the “Log in” link in the header bar
Enter the correct password in the password field with no username in the username field and click on ‘Log in’ button.

Expected result:
The user is unable to login successfully, and the following error message is displayed below the username field - “Please enter your Spotify username or email address”

Test data: 
User name/password: myspotifytestuser@gmail.com


Scaling the Test suite and handling test data:

The list of test cases mentioned above covers the basic ‘Login’ functionality of Spotify (excludes Social login). I have now successfully automated two of the above mentioned test cases. The framework is based on Cucumber and Selenium WebDriver API which is robust and highly scalable. Here we make use of the “Page Object model” for modularization and scalability. This gives us the capability to add additional tests with fewer modifications when new features are added. Cucumber not only helps us in achieving readability, but also helps us in maintaining and organizing input test data, where we can embed the test data in the scenarios if needed. In this case, since we are testing only the ‘Login’ functionality, and the test data is fairly minimal, I haven’t used any property file or an external data source to handle test data. 

Handling Cross browser compatibility:

In this case, the framework is structured in such a way that the tests can be run locally in chrome browser using the chromedriver binary. But, we can also run multiple tests parallely in multiple browsers (Chrome, Safari, Firefox, Internet Explorer) on a cloud server such as saucelabs. To achieve this, we can instantiate a ‘DesiredCapabilities’ object, configure it to run with a specific browser, version and platform, and pass them as parameters while instantiating a ‘RemoteWebDriver’ object during the setup process.

Login Feature Execution steps:

 - Download or clone the repository to your local system from BitBucket
 - Import the project into your IDE such as Eclipse or IntelliJ
 - Navigate to the Login feature file in the project set up: features -> Login.feature
 - Run it as a Cucumber feature from the IDE: Run -> Run As -> Cucumber feature