# This feature tests the Login functionality of Spotify.com
Feature: Login feature of Spotify

  Scenario: Login and Log out as a normal user
    Given I visit the spotify home page
    And I navigate to the Spotify Login page
    When I login with username as "myspotifytestuser@yopmail.com" and password as "spotify123"
    Then the login is successful
    Then I logout successfully

  Scenario: Unsuccessful user authentication for a user with invalid password
    Given I visit the spotify home page
    And I navigate to the Spotify Login page
    When I login with username as "myspotifytestuser@yopmail.com" and password as "spotify12333"
    Then the login is unsuccessful
